import 'package:flutter/material.dart';
import 'package:voice_command_apps/views/widgets/button_auth.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:voice_command_apps/views/widgets/logo_app.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final TextEditingController emailTextCtrl = TextEditingController();
  final TextEditingController passwordTextCtrl = TextEditingController();
  final TextEditingController confirmPasswordTextCtrl = TextEditingController();

  bool passVisible = false;
  bool confirmPassVisible = false;

  @override
  Widget build(BuildContext context) {
    return Container();
  }

  Widget main() => SizedBox(
        height: 1.sh,
        width: 1.sw,
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(height: 20.h),
              logo(),
              SizedBox(height: 10.h),
              emailField(),
              SizedBox(height: 10.h),
              passwordField(),
              SizedBox(height: 10.h),
              confirmField(),
              SizedBox(height: 10.h),
              buttonRegister()
            ],
          ),
        ),
      );

  Widget logo() => const LogoApp();

  Widget emailField() => TextField(
        controller: emailTextCtrl,
        keyboardType: TextInputType.emailAddress,
        decoration: const InputDecoration(
          hintText: 'Input email',
          prefixIcon: Icon(Icons.email),
          border: OutlineInputBorder(),
        ),
      );

  Widget passwordField() => TextField(
        controller: passwordTextCtrl,
        obscureText: passVisible,
        decoration: const InputDecoration(
          hintText: 'Input password',
          prefixIcon: Icon(Icons.lock),
          border: OutlineInputBorder(),
        ),
      );

  Widget confirmField() => TextField(
        controller: confirmPasswordTextCtrl,
        obscureText: confirmPassVisible,
        decoration: const InputDecoration(
          hintText: 'Input konfirmasi password',
          prefixIcon: Icon(Icons.lock),
          border: OutlineInputBorder(),
        ),
      );

  Widget buttonRegister() => ButtonAuth(title: 'Register', callback: () => {});
}
