import 'package:flutter/material.dart';
import 'package:voice_command_apps/views/widgets/button_auth.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:voice_command_apps/views/widgets/logo_app.dart';

import 'package:get/get.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final TextEditingController emailTextCtrl = TextEditingController();
  final TextEditingController passwordTextCtrl = TextEditingController();

  final padding = EdgeInsets.symmetric(vertical: 10.h, horizontal: 12.w);

  final ts = TextStyle(
      color: Colors.black87, fontSize: 13.sp, fontWeight: FontWeight.w700);

  bool visiblePassword = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) => Scaffold(body: SafeArea(child: main()));

  Widget main() => Container(
        height: 1.sh,
        width: 1.sw,
        padding: padding,
        color: Colors.white,
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(height: 20.h),
              logo(),
              SizedBox(height: 20.h),
              emailField(),
              SizedBox(height: 10.h),
              passwordField(),
              SizedBox(height: 10.h),
              loginButton(),
            ],
          ),
        ),
      );

  Widget logo() => const LogoApp();

  Widget passwordField() => TextField(
        controller: passwordTextCtrl,
        obscureText: visiblePassword,
        decoration: const InputDecoration(
          hintText: 'Input password',
          prefixIcon: Icon(Icons.lock),
          border: OutlineInputBorder(),
        ),
      );

  Widget emailField() => TextField(
        controller: emailTextCtrl,
        keyboardType: TextInputType.emailAddress,
        decoration: const InputDecoration(
          hintText: 'Input email',
          prefixIcon: Icon(Icons.email),
          border: OutlineInputBorder(),
        ),
      );

  Widget loginButton() => ButtonAuth(title: 'Login', callback: () => {});

  Widget linkRegister() => TextButton(
      onPressed: () => Get.to,
      child: Text('Atau register terlebih dulu', style: ts));
}
