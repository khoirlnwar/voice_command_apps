import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:voice_command_apps/views/widgets/apps_theme.dart';

class ProcessBanner extends StatelessWidget {
  ProcessBanner({Key? key}) : super(key: key);

  final padding = EdgeInsets.symmetric(horizontal: 5.w);

  final mainAlignment = MainAxisAlignment.center;

  final boxDecoration = BoxDecoration(
    color: Colors.white,
    borderRadius: BorderRadius.circular(8.r),
    border: Border.all(color: AppsTheme.shadow),
  );

  @override
  Widget build(BuildContext context) => container();

  Widget container() => Container(
        width: 0.8.sw,
        height: 60.h,
        padding: padding,
        decoration: boxDecoration,
        child: Row(
          mainAxisAlignment: mainAlignment,
          children: [
            Expanded(child: record()),
            Expanded(child: classify()),
            Expanded(child: control()),
          ],
        ),
      );

  // 1. RECORD YOUR SPEECH
  Widget record() => Item(
      text: 'Record your speech',
      child: Icon(Icons.record_voice_over, color: AppsTheme.primary));

  // 2. EXTRACT & CLASSIFY ITS MEANING
  Widget classify() => Item(
      text: 'Extract its meaning',
      child: Icon(Icons.class_, color: AppsTheme.secondary));

  // 3. CONTROL THE AUTOMATION
  Widget control() => Item(
      text: 'Control automation',
      child: Icon(Icons.settings, color: AppsTheme.third));
}

class Item extends StatelessWidget {
  final String text;
  final Widget child;

  const Item({Key? key, required this.text, required this.child})
      : super(key: key);

  final mainAlg = MainAxisAlignment.start;
  final crossAlg = CrossAxisAlignment.center;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 50.h,
      child: Column(
        mainAxisAlignment: mainAlg,
        crossAxisAlignment: crossAlg,
        children: [
          Expanded(child: child),
          Expanded(
            child: Text(text,
                style: AppsTheme.tsprocess, textAlign: TextAlign.center),
          ),
        ],
      ),
    );
  }
}
