import 'package:flutter/material.dart';
import 'package:flutter_sound/flutter_sound.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:just_audio/just_audio.dart';
import 'package:permission_handler/permission_handler.dart';

import 'package:path_provider/path_provider.dart';
import 'dart:io';

// const theSource = AudioSource.microphone;

class AudioRecorderWidget extends StatefulWidget {
  const AudioRecorderWidget({Key? key}) : super(key: key);

  @override
  _AudioRecorderWidgetState createState() => _AudioRecorderWidgetState();
}

class _AudioRecorderWidgetState extends State<AudioRecorderWidget> {
  FlutterSoundPlayer myPlayer = FlutterSoundPlayer();
  FlutterSoundRecorder myRecorder = FlutterSoundRecorder();

  Codec codec_ = Codec.aacMP4;
  String path_ = 'record_file.mp4';

  void initializer() async {
    myRecorder.openRecorder().then((value) {
      // ignore: avoid_print
      print("myRecorder has opened");
    });

    myPlayer.openPlayer().then((value) {
      // ignore: avoid_print
      print("myPlayer has opened");
    });
  }

  void disposer() {
    if (myRecorder != null) myRecorder.closeRecorder();
  }

  Future record() async {
    if (myRecorder != null) {
      if (myRecorder.isStopped) {
        // ignore: avoid_print
        print("Recording start");

        PermissionStatus status = await Permission.microphone.request();
        if (status != PermissionStatus.granted) {
          throw RecordingPermissionException(
              "Microphone permission is not granted");
        } else {
          Directory tempDir = await getTemporaryDirectory();
          String tempPath = tempDir.path;

          // print(tempPath + "");
          tempPath = tempPath + "/recorded";
          // ignore: avoid_print
          print(tempPath);

          await myRecorder
              .startRecorder(
                toFile: path_,
                codec: codec_,
                // audioSource:
                // audioSource: AudioSource(),
              )
              .whenComplete(() => print("Complete recording"));
        }
      }
    } else {
      // ignore: avoid_print
      print("myRecorder is null");
    }

    setState(() {});
  }

  Future stopRecord() async {
    myRecorder.stopRecorder().then((value) {
      // ignore: avoid_print
      print("Recorder is stopped");
      setState(() {});
    });
  }

  Future playRecord() async {
    if (myRecorder.isStopped) {
      // ignore: avoid_print
      print("myPlayer start player");
      await myPlayer.startPlayer(fromURI: path_);
    }
  }

  @override
  void initState() {
    super.initState();
    initializer();
  }

  @override
  void dispose() {
    disposer();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 160.h,
      width: 1.sw,
      padding: EdgeInsets.symmetric(vertical: 10.h, horizontal: 12.w),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              startRecordBtn(),
              SizedBox(width: 10.w),
              stopRecordBtn(),
            ],
          ),
          SizedBox(height: 10.h),
          playRecordBtn(),
        ],
      ),
    );
  }

  Widget startRecordBtn() => InkWell(
        onTap: () => record(),
        child: CircleAvatar(
          radius: 20.r,
          backgroundColor: Colors.white,
          child: Icon(Icons.mic,
              color: (myRecorder.isRecording) ? Colors.red : Colors.green),
        ),
      );

  Widget stopRecordBtn() => InkWell(
        onTap: () => (myRecorder.isStopped) ? null : stopRecord(),
        child: CircleAvatar(
          radius: 20.r,
          backgroundColor: Colors.white,
          child: Icon(
            Icons.stop,
            color: (myRecorder.isStopped) ? Colors.grey : Colors.black,
          ),
        ),
      );

  Widget playRecordBtn() => InkWell(
        onTap: () => (myRecorder.isStopped) ? playRecord() : null,
        child: CircleAvatar(
          radius: 20.r,
          backgroundColor: Colors.transparent,
          child: Icon(
            Icons.play_arrow,
            color: (myRecorder.isStopped) ? Colors.black : Colors.grey,
          ),
        ),
      );
}
