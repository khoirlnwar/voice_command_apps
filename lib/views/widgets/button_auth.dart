import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:voice_command_apps/views/widgets/apps_theme.dart';

class ButtonAuth extends StatefulWidget {
  final String title;
  final VoidCallback callback;

  const ButtonAuth({Key? key, required this.title, required this.callback})
      : super(key: key);

  @override
  _ButtonAuthState createState() => _ButtonAuthState();
}

class _ButtonAuthState extends State<ButtonAuth> {
  @override
  Widget build(BuildContext context) => button();

  Widget button() => InkWell(
        onTap: () => widget.callback(),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(4.r),
            color: AppsTheme.primary,
          ),
          height: 40.h,
          width: double.infinity,
          child: Center(child: Text(widget.title, style: AppsTheme.auth)),
        ),
      );
}
