import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class AppsTheme {
  static TextStyle auth = TextStyle(
      color: Colors.white, fontSize: 14.sp, fontWeight: FontWeight.w700);

  static TextStyle tsusername = TextStyle(
      color: Colors.white, fontSize: 15.sp, fontWeight: FontWeight.w700);

  static TextStyle tsprocess = TextStyle(
      color: Colors.black54, fontSize: 11.sp, fontWeight: FontWeight.w600);

  static Color shadow = Colors.black12;

  static Color primary = Colors.deepOrange;

  static Color secondary = Colors.orange;

  static Color third = Colors.yellowAccent;
}
