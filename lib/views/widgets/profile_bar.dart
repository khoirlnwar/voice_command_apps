import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:voice_command_apps/views/widgets/apps_theme.dart';

class ProfileBar extends StatefulWidget {
  const ProfileBar({Key? key}) : super(key: key);

  @override
  _ProfileBarState createState() => _ProfileBarState();
}

class _ProfileBarState extends State<ProfileBar> {
  @override
  Widget build(BuildContext context) => SizedBox(
        height: 45.h,
        width: 150.w,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            photo(),
            SizedBox(width: 10.w),
            nama(),
          ],
        ),
      );

  Widget photo() => CircleAvatar(
      radius: 18.r,
      backgroundColor: Colors.white,
      child: Icon(Icons.person, color: AppsTheme.secondary, size: 14.sp));

  Widget nama() => Text("Halo user", style: AppsTheme.tsusername);
}
