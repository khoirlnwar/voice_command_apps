import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class LogoApp extends StatelessWidget {
  const LogoApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => logo();

  Widget logo() => Image(
      image: const AssetImage("assets/images/logo.PNG"),
      width: 120.w,
      height: 180.h);
}
