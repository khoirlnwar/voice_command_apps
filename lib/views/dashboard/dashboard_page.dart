import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:voice_command_apps/views/widgets/apps_theme.dart';
import 'package:voice_command_apps/views/widgets/audio_recorder.dart';
import 'package:voice_command_apps/views/widgets/process_banner.dart';
import 'package:voice_command_apps/views/widgets/profile_bar.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';

class DashboardPage extends StatefulWidget {
  const DashboardPage({Key? key}) : super(key: key);

  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  final padding = EdgeInsets.only(left: 17.w, top: 15.h);

  final mainAlignment = MainAxisAlignment.start;
  final crossAlignment = CrossAxisAlignment.start;

  final decoration = BoxDecoration(
    borderRadius: BorderRadius.only(
      bottomLeft: Radius.circular(20.r),
      bottomRight: Radius.circular(20.r),
    ),
    color: AppsTheme.secondary,
  );

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle.dark,
      child: Scaffold(
        body: SafeArea(
          child: Container(
            color: Colors.white,
            height: 1.sh,
            width: 1.sw,
            child: Stack(
              children: [
                mainColumn(),
                processBanner(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget mainColumn() => Column(
        mainAxisAlignment: mainAlignment,
        crossAxisAlignment: crossAlignment,
        children: [
          header(),
          SizedBox(height: 10.h),
          audioRecorder(),
        ],
      );

  Widget header() => Container(
        width: 1.sw,
        height: 150.h,
        decoration: decoration,
        padding: padding,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            profile(),
          ],
        ),
      );

  Widget profile() => const ProfileBar();

  Widget processBanner() => Positioned(
        top: 110.h,
        left: 35.w,
        child: ProcessBanner(),
      );

  Widget audioRecorder() => const AudioRecorderWidget();
}
